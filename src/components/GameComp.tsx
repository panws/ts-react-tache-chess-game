import React from "react";
import { ChessType, GameStatus } from "../types/enums";
import BoardComp from "./BoardComp";
import "./GameComp.css";

interface IState {
  chesses: ChessType[];
  gameStatus: GameStatus;
  nextChess: ChessType.red | ChessType.black;
}

class GameComp extends React.Component<{}, IState> {
  state: IState = {
    chesses: [],
    gameStatus: GameStatus.gaming,
    nextChess: ChessType.red,
  };
  componentDidMount(): void {
    this.init();
  }
  // 初始化数据
  init() {
    const arr: ChessType[] = [];
    for (let i = 0; i < 9; i++) {
      arr.push(ChessType.none);
    }
    this.setState({
      chesses: arr,
      gameStatus: GameStatus.gaming,
    });
  }
  /**
   * 处理数据被点击的事件
   * 1. 判断游戏是否结束
   * 2. 点击的位置没有棋子
   */
  handleChessClick(index: number) {
    const chesses = [...this.state.chesses];
    chesses[index] = this.state.nextChess;
    this.setState((prevState) => ({
      chesses,
      nextChess:
        prevState.nextChess === ChessType.red ? ChessType.black : ChessType.red,
      gameStatus: this.getStatus(chesses, index),
    }));
  }

  /**
   * 获得状态
   */
  getStatus(chesses: ChessType[], index: number): GameStatus {
    // 1. 判断是否有一方获得胜利
    const horMin = Math.floor(index / 3) * 3;
    const verMin = index % 3;
    if (
      (chesses[horMin] === chesses[horMin + 1] &&
        chesses[horMin] === chesses[horMin + 2]) ||
      (chesses[verMin] === chesses[verMin + 3] &&
        chesses[verMin] === chesses[verMin + 6]) ||
      (chesses[0] === chesses[4] &&
        chesses[0] === chesses[8] &&
        chesses[0] !== ChessType.none) ||
      (chesses[2] === chesses[4] &&
        chesses[2] === chesses[6] &&
        chesses[2] !== ChessType.none)
    ) {
      if (chesses[index] === ChessType.red) {
        return GameStatus.redWin;
      } else {
        return GameStatus.blackWin;
      }
    }
    // 2. 判断是否平局
    if (!chesses.includes(ChessType.none)) {
      return GameStatus.equal;
    }
    // 3. 游戏正在进行
    return GameStatus.gaming;
  }

  getGameStatus() {
    switch (this.state.gameStatus) {
      case GameStatus.gaming:
        return <span className="title_gaming">游戏中</span>;
      case GameStatus.blackWin:
        return <span className="title_black">黑方胜利</span>;
      case GameStatus.redWin:
        return <span className="title_red">红方胜利</span>;
      case GameStatus.equal:
        return <span className="title_equal">平局</span>;
    }
  }

  render(): React.ReactNode {
    return (
      <div>
        <h1 className="gamename">井字棋游戏</h1>
        <div className="title">{this.getGameStatus()}</div>
        <BoardComp
          chesses={this.state.chesses}
          isGameOver={this.state.gameStatus != GameStatus.gaming}
          onClick={this.handleChessClick.bind(this)}
        />
      </div>
    );
  }
}

export default GameComp;
