import React from "react";
import { ChessType, GameStatus } from "../types/enums";

interface IProps {
  status: GameStatus;
  next: ChessType.red | ChessType.black;
}

function GameStatusComp(props: IProps) {
  return <div className="status">GameStatusComp</div>;
}

export default GameStatusComp;
