import React from "react";
import { ChessType } from "../types/enums";
import ChessComp from "./ChessComp";
import "./BoardComp.css";

interface IProps {
  chesses: ChessType[];
  isGameOver?: boolean;
  onClick?: (index: number) => void;
}

const BoardComp: React.FC<IProps> = function ({
  chesses,
  onClick,
  isGameOver = false,
}) {
  const list = chesses.map((type, i) => {
    return (
      <ChessComp
        key={i}
        type={type}
        onClick={() => {
          if (onClick && !isGameOver) {
            console.log(i);
            onClick(i);
          } else {
            console.log("游戏已经结束", i);
          }
        }}
      ></ChessComp>
    );
  });
  return <div className="board">{list}</div>;
};

export default BoardComp;

// 以前的做法设置默认值
// BoardComp.defaultProps = {
//   isGameOver: false,
// };
