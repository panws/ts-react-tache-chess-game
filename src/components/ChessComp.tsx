import React from "react";
import { ChessType } from "../types/enums";
import "./ChessComp.css";

export interface IProps {
  type: ChessType;
  children?: any;
  onClick?: () => void;
}

export default function ChessComp({ children, type, onClick }: IProps) {
  let chess = null;
  if (type === ChessType.red) {
    chess = <div className="red chess-item"></div>;
  } else if (type === ChessType.black) {
    chess = <div className="black chess-item"></div>;
  }

  return (
    <div
      className="chess"
      onClick={() => {
        if (type === ChessType.none && onClick) {
          // 没有棋子
          onClick();
        }
      }}
    >
      {chess}
      {children}
    </div>
  );
}
