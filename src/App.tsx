import React from "react";
import GameComp from "./components/GameComp";

function App() {
  return (
    <div className="chessbox">
      <GameComp />
    </div>
  );
}

// interface IState{
//   num:number
// }

// class App extends React.Component<{},IState> {
//   state ={
//     num:0
//   }
//   render(){
//     return (
//       <CountComp num = {this.state.num} onChange={n=>{
//         this.setState({
//           num:n
//         })
//       }} />
//     )
//   }
// }

export default App;
